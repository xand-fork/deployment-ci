variable "aws_region" {
  default = "us-west-2"
}

variable "availability_zone" {
  default = "us-west-2a"
}

variable "default_vpc_id" {
  default = "vpc-9a7b4efd"
}

variable "test_nets_vpc_id" {
  default = "vpc-0d6a00a694912f5fe"
}

variable "private_subnet_cidr" {
  default = "172.31.40.0/24"
}

variable "public_subnet_cidr" {
  default = "172.31.41.0/24"
}

variable "default-gw_id" {
  default = "igw-0712fa60"
}
