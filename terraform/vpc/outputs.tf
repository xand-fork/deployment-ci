output "private_subnet_id" {
  value = "${aws_subnet.private_subnet.id}"
}

output "aws_vpc_test_nets_cidr_block" {
  value = "${data.aws_vpc.test_nets.cidr_block}"
}

output "aws_vpc_default_cidr_block" {
  value = "${data.aws_vpc.default.cidr_block}"
}