terraform {
    backend "s3" {
        bucket = "transparent-terraform"
        key = "default-vpc"
        region = "us-west-2"
    }
}