variable "project_id" {
  default = "xand-dev"
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-b"
}

variable "network" {
    default = "default"
}

variable "private_subnet" {
    default = "private-clusters"
}

variable "dns_zone" {
  default = "tpfs-dev"
}

variable "vault_bucket" {
  default = "xand-dev-vault"
}

variable "encrypted_loggly_token" {
    default = "CiQAQvxVZ2f5xFoEculTJNM8JllSlD9viK7PVLqglp+sE5Kh+KISTgBaPTABZ2vViGDPuvDuO9p7EIEitHRA/aZjh8SF2Aj/WnR/VE3hMCNyPFFn77I2gwHhLqC9YiE+JzzlDmeh9YuGZq/HbuSqYf3YpjgN3g=="
}

variable "tag_name" {
  default = "transparent-vault"
}
