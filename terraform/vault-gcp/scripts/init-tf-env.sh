#!/bin/bash

set -o nounset
set -o errexit

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by humans.
    Its purpose is to initialize/re-initialize your local Octopus Terraform environment. If you have
    a Terraform environment, then it will be deleted and recreated.  Otherwise, a new Terraform
    environment will be initialized.

    Arguments
        CONFIG_FILE (Required) = Path to a JSON config file. It should contain values for bucket-name.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

CONFIG_FILE=${1:?"$(error 'CONFIG_FILE must be set' )"}

BUCKET_NAME=$(jq -r '.["bucket-name"]' "$CONFIG_FILE")

if [ $BUCKET_NAME == "null" ]
then
  printf "Given JSON missing bucket-name config. e.g. { \"bucket-name\": \"bucket123\", ...}\n"
  exit 1
fi

echo "Using bucket: $BUCKET_NAME"

pushd "${BASH_SOURCE%/*}/.."

rm -f .terraform/terraform.tfstate
terraform init -backend-config="bucket=$BUCKET_NAME"

popd
