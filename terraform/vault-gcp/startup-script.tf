module "startup-script-lib" {
  source = "git::https://github.com/terraform-google-modules/terraform-google-startup-scripts.git?ref=v0.1.0"
}

data "template_file" "startup-script-config" {
  template = "${file("${path.module}/scripts/templates/startup_script.tpl")}"
  vars = {
    vault_bucket = var.vault_bucket
    loggly_token = data.google_kms_secret.vault_store.plaintext
    workspace = terraform.workspace
    component = var.tag_name
  }
}
