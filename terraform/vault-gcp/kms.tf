data "google_kms_key_ring" "loggly" {
  name = "loggly"
  location = "global"
}

data "google_kms_crypto_key" "loggly_token" {
  name = "token"
  key_ring = "${data.google_kms_key_ring.loggly.self_link}"
}

data "google_kms_secret" "vault_store" {
  crypto_key = "${data.google_kms_crypto_key.loggly_token.self_link}"
  ciphertext = var.encrypted_loggly_token
}
