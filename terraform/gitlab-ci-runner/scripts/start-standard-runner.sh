#!/usr/bin/env bash

set -o errexit
set -o nounset
set -x

PROJECT_REGISTRATION_TOKEN=$1

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "$PROJECT_REGISTRATION_TOKEN" \
  --executor "docker" \
  --docker-image alpine:3 \
  --description "docker-runner" \
  --tag-list "docker,aws" \
  --run-untagged \
  --locked="false" \
  --docker-privileged="true"

sudo gitlab-runner start