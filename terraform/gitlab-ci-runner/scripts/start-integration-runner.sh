#!/usr/bin/env bash

set -o errexit
set -o nounset

PROJECT_REGISTRATION_TOKEN=$1

sudo gitlab-runner register \
    --non-interactive \
    --url "https://gitlab.com/" \
    --registration-token "$PROJECT_REGISTRATION_TOKEN" \
    --executor "shell" \
    --description "shell-runner" \
    --tag-list "integration" \
    --locked="false"

sudo gitlab-runner start
