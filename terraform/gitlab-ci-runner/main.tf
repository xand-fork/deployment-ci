terraform {
  backend "s3" {
    bucket = "transparent-terraform"
    key = "ci-runners"
    region = "us-west-2"
  }
}
