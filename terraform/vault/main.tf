provider "aws" {
  region = "${var.region}"
}

resource "aws_key_pair" "deployer" {
  key_name = "${terraform.workspace}-deployer_key"
  public_key = "${file("deployer-key.pub")}"
}

terraform {
  backend "s3" {
    bucket = "transparent-terraform"
    key = "vault"
    region = "us-west-2"
  }
}
