#!/usr/bin/env bash

set -o nounset
set -o errexit

# Variables
SERVER_ADDR=$1
LOGGLY_TOKEN=$2
WORKSPACE=$3
COMPONENT=$4

# sleep until instance is ready
until [ -f /var/lib/cloud/instance/boot-finished ]; do
    sleep 1
done

sleep 20

sudo tee -a /etc/rsyslog.d/60-server.conf <<EOF
# Setup Loggly logging template
template(name="LogglyFormat" type="string"
  string="<%pri%>%protocol-version% %timestamp:::date-rfc3339% %HOSTNAME% %app-name% %procid% %msgid% [${LOGGLY_TOKEN}@41058 tag=\"${WORKSPACE}\" tag=\"AWS-${SERVER_ADDR}-${WORKSPACE}-${COMPONENT}\"] %msg%\n")

# Send logs to Loggly
*.* action(type="omfwd" protocol="tcp" target="logs-01.loggly.com" port="514" template="LogglyFormat")


EOF

sudo systemctl restart rsyslog.service



