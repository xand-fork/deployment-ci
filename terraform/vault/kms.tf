data "aws_kms_secrets" "vault_store" {
  secret {
    name = "loggly-acct-token"
    payload = "AQICAHj63riLdT/j8YpQFQKejFovZewcwh2BVBH4jdAR+/uRQgEJgu0beRebWHDohR8hGl6dAAAAgzCBgAYJKoZIhvcNAQcGoHMwcQIBADBsBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDD+wgqR4s1/CyrN7egIBEIA/IBJB133pKQ5QA0/n7eJtchMq7e7PYMECoSdxVM5Pu55ntKXkNlnz2U6k5oFDVDvK6nmqurU/ZS7iqOgYwOd/"
  }
}
