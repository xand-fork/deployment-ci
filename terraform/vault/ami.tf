data "aws_ami" "vault_ami" {
  filter {
    name = "state"
    values = ["available"]
  }

  filter {
    name = "tag:Component"
    values = ["transparent-vault"]
  }

  most_recent = true
  owners = ["self"]
}