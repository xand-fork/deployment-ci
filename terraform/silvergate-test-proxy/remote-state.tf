data "terraform_remote_state" "default-vpc" {
  backend = "s3"
  workspace = "${terraform.workspace}"
  config {
    bucket = "transparent-terraform"
    key = "default-vpc"
    region = "us-west-2"
  }
}