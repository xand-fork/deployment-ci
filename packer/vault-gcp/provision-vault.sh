#!/usr/bin/env bash

set -o nounset
set -o errexit

VERSION=1.0.1

sudo apt-get update -y
sudo apt-get install -y unzip

curl -L "https://releases.hashicorp.com/vault/${VERSION}/vault_${VERSION}_linux_amd64.zip" > /tmp/vault.zip

cd /tmp
sudo unzip vault.zip
sudo mv vault /usr/local/bin
sudo chmod 0755 /usr/local/bin/vault
sudo chown root:root /usr/local/bin/vault

sudo mkdir /etc/vault
sudo cp /tmp/config.hcl /etc/vault/config.hcl
sudo chown root:root /etc/vault/config.hcl

sudo mkdir /etc/scripts
sudo cp /tmp/configure-logging.sh /etc/scripts/configure-logging.sh
sudo chown root:root /etc/scripts/configure-logging.sh

sudo cp /tmp/configure-vault.sh /etc/scripts/configure-vault.sh
sudo chmod +x /etc/scripts/configure-vault.sh

sudo cp /tmp/vault.service /lib/systemd/system/vault.service
sudo chown root:root /lib/systemd/system/vault.service

sudo systemctl enable vault.service
