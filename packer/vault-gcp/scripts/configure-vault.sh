#!/usr/bin/env bash

set -o nounset
set -o errexit

VAULT_BUCKET=$1

sed -i"" -e "s/{TERRAFORM_INSERT_VAULT_BUCKET}/$VAULT_BUCKET/g" /etc/vault/config.hcl