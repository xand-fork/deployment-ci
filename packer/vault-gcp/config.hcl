storage "gcs" {
  bucket = "{TERRAFORM_INSERT_VAULT_BUCKET}"
}

listener "tcp" {
  address       = "0.0.0.0:8200"
  tls_cert_file = "/etc/vault/vault.crt"
  tls_key_file = "/etc/vault/vault.key"
}

ui = true
default_lease_ttl = "4320h"
max_lease_ttl = "4320h"
