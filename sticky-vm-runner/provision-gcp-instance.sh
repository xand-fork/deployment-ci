#!/usr/bin/env bash

terraform apply -auto-approve .
# gcloud config set project xand-dev
#GCP_PROJECT="xand-dev"
#VM_NAME="sticky-vm-runner"
#DESCRIPTION="GitLab runner with persistence storage for optimized cargo caching"
#DISK_SIZE="200GB"

# Machine type must be available in zone
#MACHINE_TYPE="n2-highcpu-16" # 16 cores + 16GB RAM
#ZONE="us-central1-a"

#IMAGE_FAMILY="cos-stable"
#IMAGE_PROJECT="cos-cloud"

#NETWORK="projects/xand-dev/global/networks/default"
#SUBNET="projects/xand-dev/regions/us-central1/subnetworks/private-clusters"

gcloud compute instances create $VM_NAME  \
  --description="$DESCRIPTION" \
  --boot-disk-size=$DISK_SIZE \
  --machine-type=$MACHINE_TYPE \
  --zone=$ZONE \
  --image-family=$IMAGE_FAMILY \
  --image-project=$IMAGE_PROJECT \
  --scopes https://www.googleapis.com/auth/devstorage.read_write \
  --network-interface=no-address,network=$NETWORK,subnet=$SUBNET \
  --project=$GCP_PROJECT


