resource "google_compute_instance" "default" {
  name         = var.vm_name
  description  = var.description
  machine_type = var.machine_type
  zone         = var.zone
  project      = var.gcp_project

  tags = []
  boot_disk {
    initialize_params {
      image = "${var.image_project}/${var.image_family}"
      size  = var.disk_size
    }
  }

  network_interface {
    network    = var.network
    subnetwork = var.subnet
  }

  service_account {
    scopes = ["storage-rw"]
  }
}
