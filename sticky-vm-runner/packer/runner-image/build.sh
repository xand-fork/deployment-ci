#!/usr/bin/env bash

if [[ -z $GITLAB_REGISTRATION_TOKEN ]] ; then
    echo "ERROR: GITLAB_REGISTRATION_TOKEN must be set. Set it in the environment or before calling script with GITLAB_REGISTRATION_TOKEN=xxx ./build.sh"
    exit 1
fi

packer build -var "gitlab_registration_token=${GITLAB_REGISTRATION_TOKEN}" sticky-vm-runner.json
