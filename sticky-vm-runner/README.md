# Deploying the Sticky VM runner

The sticky VM runner build and deploy process consists of 
1. Building a GCP VM Image with required dependencies (docker, docker-credential-gcr, gitlab-runner)
1. Building a GCP VM Image of a registered gitlab-runner against the top-level TransparentDevelopmentInc group
1. Deploying 1 instance of the registered runner via terraform

## Pre-reqs

You must have `packer`, `terraform`, and `gcloud` installed. 

Ensure you are logged in with `gcloud`:
```
gcloud auth login
``` 
And generate an application token with:
```sh
gcloud auth application-default login
```

## Steps

Update `terraform/main.tf` to reference your generated application token. `~` or `$HOME` does not resolve within the terraform process.

Fetch a GitLab registration token for the TransparentIncDevelopment group [here](https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd) (Expand `Runners` to see the token)

Run the following script

```sh
GITLAB_REGISTRATION_TOKEN=<insert the token here>

# Build image 1
pushd packer/dependency-image
./build.sh
popd

# Build image 2
pushd packer/runner-image
GITLAB_REGISTRATION_TOKEN=$GITLAB_REGISTRATION_TOKEN ./build.sh
popd

# Deploy VM instance
pushd terraform
./deploy.sh
popd
```


## Unpause runner

The runner instance starts in a "paused" state by default. Visit:  
https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd  

Expand "Runners", and click on the correct `sticky-docker-runner` to unpause it, so GitLab will let it pick up jobs.
> Note: Paused vs unpaused lives only on the GitLab side of things. If a runner instance is not running, GitLab may think it is "unpaused" until someone explicitly
> removes it or it crosses some long-term timeout threshold (many weeks)

## Runbook

Visit this runbook to see tips for how to debug or diagnose runner issues: https://transparentinc.atlassian.net/wiki/spaces/XNS/pages/394395689/Sticky+VM+Runbook
