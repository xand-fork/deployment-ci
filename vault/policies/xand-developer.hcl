
path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "/auth/token/create" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "/auth/token/revoke" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "/auth/token/lookup" {
  capabilities = ["read"]
}
