
path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "/auth/token/create" {
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "secret/environments/prod/*" {
  capabilities = ["deny"]
}

