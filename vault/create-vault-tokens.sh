#!/usr/bin/env bash

set -e

POLICY_NAME=${1:?"Policy name is required"}
TOKEN_COUNT=${2:?"You must specify a number of tokens"}

ROOT_TOKEN=$(cat ~/.vault-token)

for i in $(seq 1 $TOKEN_COUNT);
do
    ONE_TIME_TOKEN=$(vault token create -use-limit=2 -policy=default -orphan -format=json | jq -r .auth.client_token)
    if [[ $POLICY_NAME -eq "xand-developer" ]];
    then
        RENEWABLE="true"
    else
        RENEWABLE="false"
    fi
    USER_TOKEN_JSON=$(vault token create -policy=$POLICY_NAME -period "4320h" -renewable="$RENEWABLE" -orphan -format=json)
    USER_TOKEN=$(echo "$USER_TOKEN_JSON" | jq -r .auth.client_token)
    USER_TOKEN_ACCESSOR=$(echo "$USER_TOKEN_JSON" | jq -r .auth.accessor)

    echo $ONE_TIME_TOKEN > ~/.vault-token

    vault write cubbyhole/$ONE_TIME_TOKEN token=$USER_TOKEN > /dev/null
    echo "One time token: $ONE_TIME_TOKEN Revocation accessor: $USER_TOKEN_ACCESSOR"
    echo $ROOT_TOKEN > ~/.vault-token
done

echo "Don't forget to invalidate the root token when finished"