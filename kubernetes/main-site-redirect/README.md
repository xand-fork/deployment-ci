# Nginx Redirect for Main Website
This is currently deployed to the development cluster in gke `xand-production` project. This deployment redirects from ancillary urls to the main website of transparent.us. You will need to be on the VPN in order for the kubectl commands in this readme to work.

# Deploying Configuration

Connect to the development cluster then just apply the kubneretes configuration.

```shell
gcloud container clusters get-credentials --project xand-production production

# Apply kubernetes configuration from this directory.
kubectl apply -f 00-namespace.yaml
kubectl apply -f 01-deployment.yaml
kubectl apply -f 02-ingress.yaml
```
