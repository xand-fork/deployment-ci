apiVersion: apps/v1
kind: Deployment
metadata:
  name: zulip
  labels:
    app: zulip
spec:
  replicas: 1
  selector:
    matchLabels:
      app: zulip
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: zulip
    spec:
      initContainers:
        - name: file-perms
          image: busybox
          command: ['chmod', '-R', '777', '/backups']
          volumeMounts:
          - name: zulip-backup-storage
            mountPath: /backups
      containers:
      - name: zulip
        image: zulip/docker-zulip:2.0.0-0
        resources:
          limits:
            cpu: 150m
            memory: 5Gi
        env:
        # Please take a look at the environment variables in docker-compose.yml for all required env variables!
        - name: DB_HOST
          value: 'zulip-postgresql'
        - name: SETTING_MEMCACHED_LOCATION
          value: 'zulip-memcached:11211'
        - name: SETTING_REDIS_HOST
          value: 'zulip-redis'
        - name: SETTING_RABBITMQ_HOST
          value: 'zulip-rabbitmq'
        - name: ZULIP_AUTH_BACKENDS
          value: 'EmailAuthBackend,GoogleMobileOauth2Backend'
        - name: SETTING_EXTERNAL_HOST
          value: 'chat.tpfs.io'
        - name: SETTING_ZULIP_ADMINISTRATOR
          valueFrom:
            secretKeyRef:
              name: zulip
              key: zulip-user-email
        - name: SETTING_EMAIL_HOST
          valueFrom:
            secretKeyRef:
              name: zulip
              key: smtp-url
        - name: SETTING_EMAIL_USE_TLS
          value: "True"
        - name: SETTING_EMAIL_PORT
          value: "587"
        - name: SETTING_EMAIL_HOST_USER
          valueFrom:
            secretKeyRef:
              name: zulip
              key: smtp-user
        - name: SECRETS_EMAIL_PASSWORD
          valueFrom:
            secretKeyRef:
              name: zulip
              key: smtp-password
        - name: ZULIP_USER_EMAIL
          valueFrom:
            secretKeyRef:
              name: zulip
              key: zulip-user-email
        - name: ZULIP_USER_DOMAIN
          value: 'transparentsystems.com'
        - name: ZULIP_USER_PASS
          valueFrom:
            secretKeyRef:
              name: zulip
              key: zulip-user-password
        - name: SECRETS_secret_key
          valueFrom:
            secretKeyRef:
              name: zulip
              key: zulip-secret-key
        # These should match the passwords configured above
        - name: DB_USER
          valueFrom:
            secretKeyRef:
              name: zulip
              key: postgresql-user
        - name: SECRETS_postgres_password
          valueFrom:
            secretKeyRef:
              name: zulip
              key: postgresql-password
        - name: SETTING_RABBITMQ_USER
          valueFrom:
            secretKeyRef:
              name: zulip
              key: rabbitmq-user
        - name: SECRETS_rabbitmq_password
          valueFrom:
            secretKeyRef:
              name: zulip
              key: rabbitmq-password
        - name: SETTING_GOOGLE_OAUTH2_CLIENT_ID
          valueFrom:
            secretKeyRef:
              name: zulip
              key: google-oauth2-client-id
        - name: SECRETS_google_oauth2_client_secret
          valueFrom:
            secretKeyRef:
              name: zulip
              key: google-oauth2-client-secret
        - name: DISABLE_HTTPS
          value: 'True'
        - name: SSL_CERTIFICATE_GENERATION
          value: 'self-signed'
        - name: SETTING_PUSH_NOTIFICATION_BOUNCER_URL
          value: 'https://push.zulipchat.com'
        - name: SETTING_PUSH_NOTIFICATION_REDACT_CONTENT
          value: 'True'
        ports:
        - containerPort: 80
          name: http
          protocol: TCP
        volumeMounts:
          - name: zulip-persistent-storage
            mountPath: /data
          - name: zulip-backup-storage
            mountPath: /backups
      volumes:
      - name: zulip-persistent-storage
        persistentVolumeClaim:
          claimName: zulip
      - name: zulip-backup-storage
        persistentVolumeClaim:
          claimName: zulip-backup
---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis
  labels:
    app: zulip-redis
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: zulip-redis
  template:
    metadata:
      labels:
        app: zulip-redis
    spec:
      containers:
      - name: redis
        image: quay.io/sameersbn/redis:latest
        resources:
          limits:
            cpu: 50m
        volumeMounts:
          - name: redis-persistent-storage
            mountPath: /var/lib/redis
        ports:
        - containerPort: 6379
          name: redis
          protocol: TCP
      volumes:
      - name: redis-persistent-storage
        persistentVolumeClaim:
          claimName: redis
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: memcached
  labels:
    app: zulip-memcached
spec:
  replicas: 1
  selector:
    matchLabels:
      app: zulip-memcached
  template:
    metadata:
      labels:
        app: zulip-memcached
    spec:
      containers:
      - name: memcached
        image: quay.io/sameersbn/memcached:latest
        resources:
          limits:
            cpu: 75m
            memory: 768Mi
        ports:
        - containerPort: 11211
          name: memcached
          protocol: TCP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rabbitmq
  labels:
    app: zulip-rabbitmq
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: zulip-rabbitmq
  template:
    metadata:
      labels:
        app: zulip-rabbitmq
    spec:
      containers:
      - name: rabbitmq
        image: rabbitmq:3.7.7
        resources:
          limits:
            cpu: 75m
            memory: 768Mi
        env:
        - name: RABBITMQ_DEFAULT_USER
          valueFrom:
            secretKeyRef:
              name: zulip
              key: rabbitmq-user
        - name: RABBITMQ_DEFAULT_PASS
          valueFrom:
            secretKeyRef:
              name: zulip
              key: rabbitmq-password
        volumeMounts:
          - name: rabbitmq-persistent-storage
            mountPath: /var/lib/rabbitmq
        ports:
        - containerPort: 5672
          name: rabbitmq
          protocol: TCP
      volumes:
      - name: rabbitmq-persistent-storage
        persistentVolumeClaim:
          claimName: rabbitmq
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgresql
  labels:
    app: zulip-postgresql
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: zulip-postgresql
  template:
    metadata:
      labels:
        app: zulip-postgresql
    spec:
      containers:
      - name: postgresql
        image: zulip/zulip-postgresql
        resources:
          limits:
            cpu: 80m
            memory: 768Mi
        env:
        - name: POSTGRES_DB
          value: zulip
        - name: PGDATA
          value: /var/lib/postgresql/data/pgdata
        - name: POSTGRES_USER
          valueFrom:
            secretKeyRef:
              name: zulip
              key: postgresql-user
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              name: zulip
              key: postgresql-password
        volumeMounts:
          - name: postgresql-persistent-storage
            mountPath: /var/lib/postgresql/data/
        ports:
        - containerPort: 5432
          name: redis
          protocol: TCP
      volumes:
      - name: postgresql-persistent-storage
        persistentVolumeClaim:
          claimName: postgresql
